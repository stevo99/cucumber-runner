# running headless cukes

```sh
docker run -it -v /root/ruby_cuke_demo:/opt/app-root/src cucumber-runner bash -c 'source /opt/rh/rh-ruby22/enable ; export PATH=$PATH:/opt/rh/rh-ruby22/root/usr/local/bin ; Xvfb :99 -ac & bundle ; bundle exec rake features:all'
```
